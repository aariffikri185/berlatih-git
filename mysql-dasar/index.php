<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    
<?php
$trainer = array (
  array("Rezky","Laravel"),
  array("Abduh","Adonis"),
  array("Iqbal","VueJs"),
);

echo "<pre>";
print_r($trainer);
echo "</pre>";

function pagar_bintang($integer){
  
  for($i = 1; $i<=$integer;$i++){

  for($j=1; $j<=$integer;$j++){
  
  $hasil=$i%2;
  
  if($hasil ==1){
  
  echo "#";}
  
  else{
  
  echo "*";};}
  echo "<br>";}}

  pagar_bintang(5);

  $sebelum = "Aku+akan+menjadi+developer+handal+di+masa+depan+!<br>";

  echo $sebelum;
  
  $sesudah = str_replace("+"," ",$sebelum);
  
  echo $sesudah;

  $data = array(

    "siswa_pertama" => array("id" => "001", "nama" => "Abduh", "kelas" => "Adonis", "nilai" => 70),
    
    "siswa_kedua" => array("id" => "002", "nama" => "Rezky", "kelas" => "Laravel", "nilai" => 90),
    
    "siswa_ketiga" => array("id" => "003", "nama" => "Iqbal", "kelas" => "VueJS", "nilai" => 85),
    
    "siswa_keempat" => array("id" => "004", "nama" => "Alim", "kelas" => "Golang", "nilai" => 80)
    
    );
  print_r($data);
  echo "<br>";

// Siswa dengan ID 003 sedang belajar Adonis

// Rezky telah menyelesaikan Golang dengan nilai 85

// Kelas VueJS diikuti oleh Abduh dan Alim



  echo "Siswa dengan ID ".$data["siswa_ketiga"]["id"], " sedang belajar " .$data["siswa_pertama"]["kelas"], "<br>";
  echo $data["siswa_kedua"]["nama"]," telah menyelesaikan " .$data["siswa_keempat"]["kelas"], " dengan nilai " .$data["siswa_ketiga"]["nilai"], "<br>";
  echo "Kelas " .$data["siswa_ketiga"]["kelas"], " diikuti oleh " .$data["siswa_pertama"]["nama"], " dan " .$data["siswa_keempat"]["nama"], "<br>";
?>

<!-- //output

Array
(
    [0] => Array
        (
            [0] => Rezky
            [1] => Laravel
        )

    [1] => Array
        (
            [0] => Abduh
            [1] => Adonis
        )

    [2] => Array
        (
            [0] => Iqbal
            [1] => VueJs
        )

) -->


</body>
</html>